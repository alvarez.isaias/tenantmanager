import {InputType} from 'reactstrap/lib/Input'

type APIMethods = 'GET' | 'POST' | 'PUT'
export type APIMethod = APIMethods

export interface IHttpResponse<T> extends Response {
  parsedBody?: T
}

export interface TenantUser {
  tenantUserId: number
  tenantUserName: string
}

export interface Tenant {
  tenantId: number
  tenantName: string
  letterhead: string
  isDefault: boolean
  isActive: boolean
  users: TenantUser[]
  buyerEmail?: string
}

export class TenantInput {
  public tenantName: string = ''
  public letterhead: string = ''
  public isDefault: boolean = false
  public isActive: boolean = true
  public buyerEmail?: string = ''
}

export class TenantUserInput {
  public tenantUserName: string = ''
}

/* istanbul ignore next */
export class TenantUserEditInput {
  public tenantId: number = 0
  public tenantUserName: string = ''

  constructor(tenantId: number, tenantUserName: string) {
    this.tenantId = tenantId
    this.tenantUserName = tenantUserName
  }
}

export type AlertType = 'info' | 'danger'

export const alertTypes: {[key: string]: AlertType} = {
  ERROR: 'danger',
  INFO: 'info',
}

export interface FormInput {
  label: string
  name: string
  rows?: string
  type?: InputType
  invalid?: boolean
  required?: boolean
  regex?: RegExp
}
