import {fireEvent, render, wait} from '@testing-library/react'
import React from 'react'
import {Tenant} from '../../../apiService'
import mockTenants from '../../../apiService/__mocks__/mocks'
import {AlertsContextProvider} from '../../../contexts/alerts'
import TenantRow from '../TenantRow'

const mockTenant: Tenant = mockTenants[0]

describe('<TenantRow />', () => {
  test('renders tenants', () => {
    const {getByText} = render(
      <TenantRow
        editTenant={jest.fn()}
        tenant={mockTenant}
      />
    )

    expect(getByText(mockTenant.tenantId.toString())).not.toBeNull()
    expect(getByText(mockTenant.tenantName)).not.toBeNull()
    expect(getByText('Yes')).not.toBeNull()
  })
  test('renders tenant users when clicking the open button', async () => {
    const {getByTestId} = render(
      <AlertsContextProvider>
        <TenantRow
          editTenant={jest.fn()}
          tenant={mockTenant}
        />
      </AlertsContextProvider>
    )
    const showUsersButton = getByTestId('show-users') as HTMLElement

    expect(showUsersButton).not.toBeNull()
    fireEvent.click(showUsersButton)
    await wait(() => expect(getByTestId('collapsable-body')).not.toBeNull())
  })
})
