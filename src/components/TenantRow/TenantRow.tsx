import {faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import React, {useState} from 'react'
import {Card, CardBody, CardHeader, Collapse} from 'reactstrap'
import {Tenant} from '../../apiService/utils/types'
import Actions from '../Actions/Actions'
import TenantUsers from '../TenantUsers/TenantUsers'
import {DefinitionList} from '../shared'
import {getTenantDefinitionList} from '../utils/tenant'
import styles from './TenantRow.module.scss'

interface TenantRowProps {
  tenant: Tenant
  editTenant: (tenant: Tenant) => void
}

const TenantRow = ({tenant, editTenant}: TenantRowProps) => {
  const [open, setOpen] = useState(false)
  const handleRowClick = () => setOpen(!open)

  return (
    <Card key={tenant.tenantId}>
      <CardHeader>
        <div className={styles.tenantRow}>
          <DefinitionList
            list={getTenantDefinitionList(tenant)}
          />
          <Actions<Tenant>
            onConfirmAction={editTenant}
            data={tenant}
            modalTitle='Edit tenant'
          />
        </div>
        <div
          className={`${styles.divider} ${styles.clickable}`}
          onClick={handleRowClick}
          data-testid='show-users'
        >
          <FontAwesomeIcon icon={open ? faChevronUp : faChevronDown} />
        </div>
      </CardHeader>
      <Collapse isOpen={open}>
        {
          open &&
            <CardBody data-testid='collapsable-body'>
              <TenantUsers
                tenantId={tenant.tenantId}
              />
            </CardBody>
        }
      </Collapse>
    </Card>
  )
}

export default TenantRow
