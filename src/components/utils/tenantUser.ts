import { InputType } from 'reactstrap/lib/Input'
import {
  endpoints,
  http,
  TenantUser,
  TenantUserEditInput,
  TenantUserInput,
} from '../../apiService'
import {DefinitionListElement} from '../shared'

export const getTenantUserList = async (tenantId: number) => {
  try {
    return await http<TenantUser[]>(
      `${endpoints.tenants_endpoint}${tenantId}/users`,
      'GET',
    )
  } catch (err) {
    return err
  }
}

export const addNewTenantUser = async (newTenantUser: TenantUserInput, tenantId: number) => {
  try {
    return await http<TenantUserInput>(
      `${endpoints.tenants_endpoint}${tenantId}/users`,
      'POST',
      newTenantUser,
    )
  } catch (err) {
    return err
  }
}

export const tenantUserPut = async(tenantUser: TenantUser, tenantId: number) => {
  try {
    return await http<TenantUserEditInput>(
      `${endpoints.tenant_users_endpoint}${tenantUser.tenantUserId}`,
      'PUT',
      new TenantUserEditInput(tenantId, tenantUser.tenantUserName),
    )
  } catch (err) {
    return err
  }
}

export const getTenantUserDefinitionList = (user: TenantUser): DefinitionListElement[] => {
  return [
    {dt: 'User Id', dd: user.tenantUserId.toString()},
    {dt: 'User Name', dd: user.tenantUserName}
  ]
}

export const userInputs = [
  {label: 'Tenant User Name', name: 'tenantUserName', type: 'text' as InputType, required: true},
]

export const filterUsers = (users: TenantUser[], searchQuery: string): TenantUser[] => (
  users.filter((user: TenantUser) => (
    user.tenantUserName.toLowerCase().includes(
      searchQuery.toLowerCase(),
    )
  ))
)
