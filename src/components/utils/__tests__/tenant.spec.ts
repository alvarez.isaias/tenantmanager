import {endpoints, http} from '../../../apiService'
import mockTenants from '../../../apiService/__mocks__/mocks'
import { getTenantDefinitionList } from '../tenant'

jest.mock('../../../apiService')

const httpMock = (http as jest.Mock)

describe('getTenantList', () => {
  test('gets the tenant list', async () => {
    httpMock.mockReturnValue(Promise.resolve({parsedBody: mockTenants}))
    const data = await http(endpoints.tenants_endpoint, 'GET')

    expect(data.parsedBody).toEqual(mockTenants)
    expect(httpMock).toHaveBeenCalledWith(endpoints.tenants_endpoint, 'GET')
  })
})
describe('addNewTenant', () => {
  test('adds new tenant', async () => {
    const mockTenant = {...mockTenants[0]}
    delete mockTenant.tenantId
    httpMock.mockReturnValue(Promise.resolve({parsedBody: mockTenant}))

    expect(http(endpoints.tenants_endpoint, 'POST', mockTenant)).resolves.toEqual(
      {parsedBody: mockTenant}
    )
    expect(httpMock).toHaveBeenCalledWith(endpoints.tenants_endpoint, 'POST', mockTenant)
  })
  test('handles error', async () => {
    const mockTenant = {...mockTenants[0]}
    delete mockTenant.tenantId
    httpMock.mockReturnValue(Promise.reject())

    expect(http(endpoints.tenants_endpoint, 'POST', mockTenant)).rejects.toBeUndefined()
  })
})
describe('tenantPut', () => {
  test('edits a tenant', async () => {
    const mockTenant = {
      ...mockTenants[0],
      tenantName: 'edited tenant',
    }
    httpMock.mockReturnValue(Promise.resolve())
    const data = await http(
      `${endpoints.tenants_endpoint}${mockTenant.tenantId}`,
      'PUT',
      mockTenant)

    expect(data).toBeUndefined()
    expect(httpMock).toHaveBeenCalledWith(
      `${endpoints.tenants_endpoint}${mockTenant.tenantId}`,
      'PUT',
      mockTenant,
    )
  })
})
describe('getTenantDefinitionList', () => {
  test('gets a tenant and maps it to a definition list element with badge', () => {
    const mockTenant = {...mockTenants[0]}

    expect(getTenantDefinitionList(mockTenant)).toStrictEqual([
      {dt: 'Tenant ID', dd: mockTenant.tenantId.toString()},
      {dt: 'Active', dd: 'Yes'},
      {
        badge: {color: 'info', content: 'Default'},
        dd: mockTenant.tenantName,
        dt: 'Tenant Name',
      },
    ])
  })
  test('gets a tenant and maps it to a definition list element without badge', () => {
    const mockTenant = {
      ...mockTenants[0],
      isDefault: false,
    }

    expect(getTenantDefinitionList(mockTenant)).toStrictEqual([
      {dt: 'Tenant ID', dd: mockTenant.tenantId.toString()},
      {dt: 'Active', dd: 'Yes'},
      {dt: 'Tenant Name', dd: mockTenant.tenantName},
    ])
  })
})
