export const endpoints = {
  tenant_users_endpoint: `${process.env.API_URL}/tenantUsers/`,
  tenants_endpoint: `${process.env.API_URL}/tenants/`,
}
