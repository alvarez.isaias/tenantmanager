const path = require('path')
const {defaults} = require('jest-config')

module.exports = {
  rootDir: path.join(__dirname, '..'),
  testEnvironment: 'jsdom',
  verbose: true,
  clearMocks: true,
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'scss', 'css'],
  testPathIgnorePatterns: [
    '/node_modules/'
  ],
  moduleNameMapper: {
    "\\.(css|scss|less)$": "identity-obj-proxy"
  },
  collectCoverage: false,
  "coverageThreshold": {
    "global": {
      "branches": 60,
      "functions": 80,
      "lines": 80
    },
    "./src/components/": {
      "branches": 40,
      "statements": 40
    },
  },
  coveragePathIgnorePatterns: [
    "<rootDir>/node_modules/",
    "<rootDir>/src/contexts",
    "<rootDir>/src/apiService/utils"
  ],
  watchPlugins: [
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname',
  ]
}
