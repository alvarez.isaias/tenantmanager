import React from 'react'
import {Badge} from 'reactstrap'
import {DefinitionListElement} from '../'
import styles from './DefinitionList.module.scss'

interface DefinitionListProps {
  list: DefinitionListElement[]
  bordered?: boolean
  extraPadding?: boolean
}

const DefinitionList = ({list, bordered, extraPadding}: DefinitionListProps) => {
  const border = bordered ? styles.bordered : ''
  const padding = extraPadding ? styles.extraPadding : ''

  return (
    <dl
      className={` ${border}`}
      data-testid='dl'
    >
      {
        list && list.map((item: DefinitionListElement, key: number) => {
          const noBorder = key === list.length - 1 ? styles.noBorder : ''

          return (
            <div key={`${item.dt}-${item.dd}`}>
              <dt
                className={`${padding} ${noBorder}`}
                data-testid='dt'
              >
                {item.dt}:
              </dt>
              <dd
                className={`${padding} ${noBorder}`}
                data-testid='dd'
              >
                {item.dd}
                {
                  item.badge &&
                    <Badge
                      color={item.badge.color}
                      pill
                      className={styles.dlBadge}
                    >
                      {item.badge.content}
                    </Badge>
                }
              </dd>
            </div>
          )
        })
      }
    </dl>
  )
}

export default DefinitionList
