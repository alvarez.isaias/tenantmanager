import {fireEvent, render, wait} from '@testing-library/react'
import React from 'react'
import {
  FormInput,
  Tenant,
  TenantInput,
  TenantUser,
  TenantUserInput,
} from '../../../apiService'
import mockTenants from '../../../apiService/__mocks__/mocks'
import {tenantInputs} from '../../utils/tenant'
import {userInputs} from '../../utils/tenantUser'
import TenantModal from '../TenantModal'

const defaultTimeout = {timeout: 2000}
const saveMock = jest.fn()
const modalMockProps = {
  data: mockTenants[0] as Partial<Tenant> | Partial<TenantUser>,
  isTenantUser: false,
  modalTitle: 'Foo',
  onSave: saveMock,
  open: true,
  saveCallback: jest.fn(),
  toggle: jest.fn(),
}

describe('<TenantModal />', () => {
  test('opens modal to edit tenant info', () => {
    const onChangeMock = jest.fn()
    const {queryByText, queryByTestId} = render(
      <TenantModal {...modalMockProps} />
    )
    const saveButton = queryByText('Save')
    expect(queryByText(modalMockProps.modalTitle)).not.toBeNull()
    expect(saveButton).not.toBeNull()
    fireEvent.click(saveButton as HTMLElement)
    wait(() => {expect(saveMock).toHaveBeenCalled()}, defaultTimeout)
    tenantInputs.forEach((input: FormInput) => {
      const inputElement = queryByTestId(input.name) as HTMLInputElement
      expect(inputElement).not.toBeNull()
      fireEvent.change(inputElement)
      wait(() => {
        expect(inputElement.value).toBe(
          modalMockProps.data[input.name],
        )
        expect(onChangeMock).toHaveBeenCalled()
      })
      expect(inputElement.value).toBe(
        modalMockProps.data[input.name]
      )
    })
  })
  test('opens modal to edit tenant user info', () => {
    modalMockProps.data = mockTenants[0].users[0]
    modalMockProps.isTenantUser = true
    const {queryByText, queryByTestId} = render(
      <TenantModal {...modalMockProps} />
    )
    const saveButton = queryByText('Save') as HTMLElement
    expect(queryByText(modalMockProps.modalTitle)).not.toBeNull()
    expect(saveButton).not.toBeNull()
    fireEvent.click(saveButton)
    wait(() => {
      expect(saveButton).toBeNull()
    })

    userInputs.forEach((input: FormInput) => {
      const inputElement = queryByTestId(input.name) as HTMLInputElement
      expect(inputElement).not.toBeNull()
      fireEvent.change(inputElement)
      expect(inputElement.value).toBe(
        modalMockProps.data[input.name],
      )
      expect(inputElement.value).toBe(
        modalMockProps.data[input.name]
      )
    })
  })
  test('opens modal to add tenant', () => {
    modalMockProps.data = new TenantInput()
    modalMockProps.isTenantUser = false
    const {queryByText, queryByTestId} = render(
      <TenantModal {...modalMockProps} />
    )
    expect(queryByText(modalMockProps.modalTitle)).not.toBeNull()

    tenantInputs.forEach((input: FormInput) => {
      const inputElement = queryByTestId(input.name) as HTMLInputElement
      expect(inputElement).not.toBeNull()
      expect(inputElement.value).toBe('')
    })
  })
  test('opens modal to add tenant user', () => {
    modalMockProps.data = new TenantUserInput()
    modalMockProps.isTenantUser = true
    const {queryByText, queryByTestId} = render(
      <TenantModal {...modalMockProps} />
    )
    expect(queryByText(modalMockProps.modalTitle)).not.toBeNull()

    userInputs.forEach((input: FormInput) => {
      const inputElement = queryByTestId(input.name) as HTMLInputElement
      expect(inputElement).not.toBeNull()
      expect(inputElement.value).toBe('')
    })
  })
})
