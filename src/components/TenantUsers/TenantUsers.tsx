import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {cloneDeep} from 'lodash'
import React, {useState} from 'react'
import {useAsync} from 'react-async-hook'
import { Input, InputGroup, InputGroupAddon, InputGroupText, Spinner } from 'reactstrap'
import {Actions} from '..'
import {TenantUser, TenantUserInput} from '../../apiService'
import {alertTypes} from '../../apiService'
import {useAlertsDisplay} from '../../contexts/alerts'
import {DefinitionList} from '../shared'
import strings from '../utils/strings'
import {
  addNewTenantUser,
  filterUsers,
  getTenantUserDefinitionList,
  getTenantUserList,
  tenantUserPut,
} from '../utils/tenantUser'
import styles from './TenantUsers.module.scss'

interface TenantUsersProps {
  tenantId: number
}

const TenantUsers = ({tenantId}: TenantUsersProps) => {
  const [users, setUsers] = useState<TenantUser[]>([])
  const [loading, setLoading] = useState(false)
  const [initialUserList, setInitialUserList] = useState<TenantUser[]>([])
  const [searchQuery, setSearchQuery] = useState('')
  const displayAlert = useAlertsDisplay()

  const asyncGetUserList = useAsync(async () => {
    setLoading(true)
    const response = await getTenantUserList(tenantId)

    if (response.parsedBody) {
      setUsers(response.parsedBody)
      setInitialUserList(response.parsedBody)
    }
    setLoading(false)
  }, [])

  const handleUserSearch = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.currentTarget.value
    setSearchQuery(value)
    searchUsers(value)
  }

  const searchUsers = (
    searchQuery: string,
  ) => {
    let clonedUsers = cloneDeep(initialUserList)
    if (searchQuery !== '') {
      clonedUsers = filterUsers(clonedUsers, searchQuery)
      setUsers(clonedUsers)
    } else {
      setUsers(initialUserList)
    }

  }

  const editTenantUser = async(tenantUser: TenantUser) => {
    try {
      await tenantUserPut(tenantUser, tenantId)
      asyncGetUserList.execute()
      displayAlert(strings.editTenantUserSuccess)
    } catch (err) {
      displayAlert(strings.editTenantUserError, {type: alertTypes.ERROR})
      return err
    }
  }

  const addTenantUser = async (newTenantUser: TenantUserInput) => {
    try {
      await addNewTenantUser(newTenantUser, tenantId)
      asyncGetUserList.execute()
      displayAlert(strings.newTenantUserSuccess)
    } catch (err) {
      displayAlert(strings.newTenantUserError, {type: alertTypes.ERROR})
      return err
    }
  }

  return (
    loading ?
      <div className={styles.spinner}>
        <Spinner
          type='grow'
          color='info'
          style={{ width: '5rem', height: '5rem' }}
        />
      </div> :
      <>
        <div className={styles.rowActions}>
          <div className={styles.inputGroup}>
            <InputGroup>
              <InputGroupAddon addonType='prepend'>
                <InputGroupText>
                  <FontAwesomeIcon icon={faSearch} />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                placeholder='Search by User Name'
                onChange={handleUserSearch}
                value={searchQuery}
              />
            </InputGroup>
          </div>
          <Actions<TenantUserInput>
            isAdd
            onConfirmAction={addTenantUser}
            data={new TenantUserInput()}
            modalTitle='Add new tenant user'
            isTenantUser
            noPadding
          />
        </div>
        {
          users.length > 0 ?
            users.map((user: TenantUser) => (
              <div
                key={user.tenantUserId}
                className={styles.tenantUserRow}
              >
                <DefinitionList
                  list={getTenantUserDefinitionList(user)}
                  extraPadding
                />
                <Actions<TenantUser>
                  onConfirmAction={editTenantUser}
                  data={{...user}}
                  secondary
                  modalTitle='Edit tenant user'
                  isTenantUser
                />
              </div>
            )) : <p className={`${styles.noResults} text-info`}>No results</p>
        }
      </>
  )
}

export default TenantUsers
