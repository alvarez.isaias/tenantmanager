import {render, wait} from '@testing-library/react'
import React from 'react'
import {Tenant} from '../../../apiService'
import mockTenants from '../../../apiService/__mocks__/mocks'
import {AlertsContextProvider} from '../../../contexts/alerts'
import {getTenantUserDefinitionList, getTenantUserList} from '../../utils/tenantUser'
import TenantUsers from '../TenantUsers'

jest.mock('../../utils/tenantUser')

const mockTenant: Tenant = mockTenants[0]
const mockUsers = mockTenant.users
const defListMock = getTenantUserDefinitionList as jest.Mock
const getUserListMock = getTenantUserList as jest.Mock

describe('<TenantUsers />', () => {
  test('gets the tenants from the API and renders a definition list', async() => {
    getUserListMock.mockResolvedValueOnce(Promise.resolve({ parsedBody: mockUsers }))
    defListMock.mockReturnValueOnce(getTenantUserDefinitionList(mockUsers[0]))
    defListMock.mockReturnValueOnce(getTenantUserDefinitionList(mockUsers[1]))
    const {
      queryByText,
      queryAllByTestId,
    } = render(
      <AlertsContextProvider>
        <TenantUsers
          tenantId={mockTenant.tenantId}
        />
      </AlertsContextProvider>,
    )

    await wait(() => {
      expect(getUserListMock).toHaveBeenCalledWith(mockTenant.tenantId)
      expect(defListMock).toHaveBeenCalledWith(mockUsers[0])
      expect(defListMock).toHaveBeenCalledWith(mockUsers[1])
      expect(queryByText('Add new tenant user')).not.toBeNull()
      expect(queryAllByTestId('dl').length).toBe(mockTenant.users.length)
      expect(queryAllByTestId('edit-button').length).toBe(mockTenant.users.length)
    })
  })
})

  test('shows No Results when there is no data', async() => {
    getUserListMock.mockResolvedValue(Promise.resolve({parsedBody: []}))
    const {queryByText} = render(
      <AlertsContextProvider>
        <TenantUsers
          tenantId={mockTenant.tenantId}
        />
      </AlertsContextProvider>,
    )

    await wait(() => {
      expect(getUserListMock).toHaveBeenCalledWith(mockTenant.tenantId)
      expect(defListMock).not.toHaveBeenCalledWith(mockUsers[0])
      expect(defListMock).not.toHaveBeenCalledWith(mockUsers[1])
      expect(queryByText('Add new tenant user')).not.toBeNull()
      expect(queryByText('No results')).not.toBeNull()
    })
})
