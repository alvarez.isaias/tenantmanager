import {orderBy} from 'lodash'
import React, {useState} from 'react'
import {useAsync} from 'react-async-hook'
import {TenantRow} from '../'
import {alertTypes, Tenant, TenantInput} from '../../apiService'
import {useAlertsDisplay} from '../../contexts/alerts'
import Actions from '../Actions/Actions'
import strings from '../utils/strings'
import {addNewTenant, getTenantList, tenantPut} from '../utils/tenant'

const TenantView = () => {
  const [tenants, setTenants] = useState<Tenant[]>([])
  const displayAlert = useAlertsDisplay()

  const asyncGetTenants = useAsync(async () => {
    const response = await getTenantList()

    if (response.parsedBody) {
      setTenants(orderBy(response.parsedBody, 'tenantId').reverse())
    }
  }, [])

  const addTenant = async (newTenant: TenantInput) => {
    try {
      await addNewTenant(newTenant)
      displayAlert(strings.newTenantSuccess)
      asyncGetTenants.execute()
    } catch (err) {
      displayAlert(strings.newTenantError, {type: alertTypes.ERROR})
      return err
    }
  }

  const editTenant = async (tenant: Tenant) => {
    try {
      await tenantPut(tenant)
      displayAlert(strings.editTenantSuccess)
      asyncGetTenants.execute()
    } catch (err) {
      displayAlert(strings.editTenantError, {type: alertTypes.ERROR})
      return err
    }
  }

  return (
    <>
      <Actions<TenantInput>
        onConfirmAction={addTenant}
        data={new TenantInput()}
        modalTitle='Add new tenant'
        isAdd
      />
      {
        tenants.map((tenant: Tenant) => (
          <TenantRow
            tenant={tenant}
            key={tenant.tenantId}
            editTenant={editTenant}
          />
        ))
      }
    </>
  )
}

export default TenantView
