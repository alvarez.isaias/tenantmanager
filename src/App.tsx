import 'babel-polyfill'
import React from 'react'

import 'bootstrap/dist/css/bootstrap.css'
import Layout from './Layout'
import {TenantView} from './components'
import {AlertsContextProvider} from './contexts/alerts'
import './styles/index.scss'

const App = () => (
  <AlertsContextProvider>
    <>
      <Layout />
      <TenantView />
    </>
  </AlertsContextProvider>
)

export default App
