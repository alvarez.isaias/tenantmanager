# Tenant Manager web application


### Install project dependencies

```sh
yarn
```

## Running the application

```sh
yarn start
```

## Run unit tests and lint

```sh
yarn test
```

## Run unit tests on watch

```sh
yarn test --watchAll
```
