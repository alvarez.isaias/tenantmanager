import {faPencilAlt} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import React, {useState} from 'react'
import {Button} from 'reactstrap'
import TenantModal from '../TenantModal/TenantModal'
import styles from './Actions.module.scss'

interface ActionsProps<T> {
  onConfirmAction: (data: T) => void
  data: T
  modalTitle: string
  isAdd?: boolean
  secondary?: boolean
  isTenantUser?: boolean
  noPadding?: boolean
}

const Actions = <T extends unknown>({
  onConfirmAction,
  isAdd,
  data,
  modalTitle,
  secondary,
  isTenantUser,
  noPadding,
}: ActionsProps<T>) => {
  const [modal, setModal] = useState(false)
  const toggleModal = () => setModal(!modal)
  const closeModal = () => setModal(false)
  return (
    <>
      <div
        className={`${styles.icon} ${secondary ? styles.secondary : ''}`}
      >
        {
          isAdd ?
            <div className={`${noPadding ? '' : styles.add}`}>
              <Button
                outline
                color='info'
                onClick={toggleModal}
                data-testid='add-button'
              >
                {modalTitle}
              </Button>
            </div> :
            <FontAwesomeIcon
              icon={faPencilAlt}
              onClick={toggleModal}
              data-testid='edit-button'
            />
        }
      </div>
      {
        modal &&
          <TenantModal<T>
            onSave={onConfirmAction}
            saveCallback={closeModal}
            modalTitle={modalTitle}
            open={modal}
            toggle={toggleModal}
            data={data}
            isTenantUser={isTenantUser}
          />
      }
    </>
  )
}

export default Actions
