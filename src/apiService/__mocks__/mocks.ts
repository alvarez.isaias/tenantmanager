const mockTenants = [
  {
    'buyerEmail': '',
    'isActive': true,
    'isDefault': true,
    'letterhead': 'FragranceNet.comTest',
    'tenantId': 1,
    'tenantName': 'TEST',
    'users': [
      {
        'tenantUserId': 1,
        'tenantUserName': 'TEST\\user1',
      },
      {
        'tenantUserId': 3,
        'tenantUserName': 'TEST\\user2',
      }
    ],
  }
]

export const mockTenantUsers = [
  {
    'tenantUserId': 1,
    'tenantUserName': 'TEST\\user1',
  },
  {
    'tenantUserId': 3,
    'tenantUserName': 'TEST\\user2',
  }
]

export const postTenantResponse = {
  'buyerEmail': 'test@test.com',
  'isActive': true,
  'isDefault': false,
  'letterhead': '',
  'tenantId': 15,
  'tenantName': 'Mock Tenant POST',
  'users': []
}

export const putTenantInput = {
  'buyerEmail': 'test1@test.com',
  'isActive': true,
  'isDefault': false,
  'letterhead': '',
  'tenantId': 15,
  'tenantName': 'Mock Tenant PUT',
  'users': []
}

export default mockTenants
