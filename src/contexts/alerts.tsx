import React from 'react'
import {Alert} from 'reactstrap'
import {AlertType} from '../apiService'
import styles from './alerts.module.scss'

interface AlertOptions {
  type?: AlertType
}

type Display = (message: string, options?: AlertOptions) => void
type Message = {text: string} & AlertOptions

const AlertsDisplayContext = React.createContext<Display | undefined>(undefined)

export const AlertsContextProvider = (props: {children: JSX.Element}) => {
  const [message, setMessage] = React.useState<Message | null>(null)

  const closeAlert = () => setMessage(null)
  const displayAlert = (displayMessage: string, options: AlertOptions = {}) => {
    setMessage({
      text: displayMessage,
      ...options,
    })
    window.setTimeout(() => {
      closeAlert()
    }, 4000)
  }

  return (
    <AlertsDisplayContext.Provider value={displayAlert}>
      {props.children}
      {
        message &&
          <div className={styles.customAlert}>
            <Alert
              color={message.type || 'info'}
              isOpen
              className={styles.fixedAlert}
            >
              {message.text}
            </Alert>
          </div>
      }
    </AlertsDisplayContext.Provider>
  )
}

export const useAlertsDisplay = () => {
  const context = React.useContext(AlertsDisplayContext)
  if (context === undefined) {
    throw new Error('useAlertsDisplay must be used within AlertsContext')
  }
  return context
}

export default AlertsDisplayContext
