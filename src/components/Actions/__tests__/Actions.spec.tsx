import {fireEvent, render, wait} from '@testing-library/react'
import React from 'react'
import {
  Tenant,
} from '../../../apiService'
import mockTenants from '../../../apiService/__mocks__/mocks'
import Actions from '../Actions'

const defaultTimeout = {timeout: 2000}
const onConfirmActionMock = jest.fn()
const modalMockProps = {
  data: mockTenants[0],
  isAdd: false,
  isTenantUser: false,
  modalTitle: 'Foo Bar',
  noPadding: false,
  onConfirmAction: onConfirmActionMock,
  secondary: false,
}

describe('<Actions  />', () => {
  test('shows component on edit mode and hides it after clicking save', () => {
    const {queryByTestId} = render(
      <Actions<Tenant> {...modalMockProps} />
    )
    const editButton = queryByTestId('edit-button')
    expect(queryByTestId('add-button')).toBeNull()
    expect(editButton).not.toBeNull()
    fireEvent.click(editButton as HTMLElement)
    // Wait 2 seconds for request to be done
    wait(() => {expect(editButton).toBeNull()}, defaultTimeout)
  })
  test('shows component on add mode and hides it after clicking save', () => {
    modalMockProps.isAdd = true
    const {queryByTestId} = render(
      <Actions<Tenant> {...modalMockProps} />
    )
    const addButton = queryByTestId('add-button')
    expect(addButton).not.toBeNull()
    expect(queryByTestId('edit-button')).toBeNull()
    fireEvent.click(addButton as HTMLElement)
    // Wait 2 seconds for request to be done
    wait(() => {expect(addButton).toBeNull()}, defaultTimeout)
  })
})
