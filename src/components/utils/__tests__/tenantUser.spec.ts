import {endpoints, http} from '../../../apiService'
import {mockTenantUsers} from '../../../apiService/__mocks__/mocks'
import {filterUsers, getTenantUserDefinitionList} from '../tenantUser'

jest.mock('../../../apiService')
const httpMock = (http as jest.Mock)

describe('getTenantUserList', () => {
  test('gets the tenant user list', async () => {
    httpMock.mockReturnValue(Promise.resolve({parsedBody: mockTenantUsers}))
    const data = await http(endpoints.tenant_users_endpoint, 'GET')
    expect(data.parsedBody).toEqual(mockTenantUsers)
    expect(httpMock).toHaveBeenCalledWith(endpoints.tenant_users_endpoint, 'GET')
  })
})

describe('addNewTenantUser', () => {
  test('adds new tenant user', async () => {
    const mockTenantUser = mockTenantUsers[0]
    delete mockTenantUser.tenantUserId
    httpMock.mockReturnValue(Promise.resolve({parsedBody: mockTenantUser}))
    const data = await http(endpoints.tenant_users_endpoint, 'POST', mockTenantUser)
    expect(data.parsedBody).toEqual(mockTenantUser)
    expect(httpMock).toHaveBeenCalledWith(endpoints.tenant_users_endpoint, 'POST', mockTenantUser)
  })
})

describe('tenantUserPut', () => {
  test('edits a tenant user', async () => {
    const mockTenantUser = {
      ...mockTenantUsers[0],
      tenantUserId: 1,
      tenantUserName: 'edited tenant',
    }
    httpMock.mockReturnValue(Promise.resolve())
    const data = await http(
      `${endpoints.tenant_users_endpoint}${mockTenantUser.tenantUserId}`,
      'PUT',
      mockTenantUser)
    expect(data).toBeUndefined()
    expect(httpMock).toHaveBeenCalledWith(
      `${endpoints.tenant_users_endpoint}${mockTenantUser.tenantUserId}`,
      'PUT',
      mockTenantUser,
    )
  })
})

describe('getTenantUserDefinitionList', () => {
  test('get a tenant user definition list', async () => {
    const mockTenantUser = {
      ...mockTenantUsers[0],
    }
    mockTenantUser.tenantUserId = 1
    const tenantUserDefinitionList = getTenantUserDefinitionList(mockTenantUser)
    expect(tenantUserDefinitionList).not.toBeNull()
    expect(tenantUserDefinitionList[0].dt).toEqual('User Id')
    expect(tenantUserDefinitionList[0].dd).toEqual(mockTenantUser.tenantUserId.toString())
    expect(tenantUserDefinitionList[1].dt).toEqual('User Name')
    expect(tenantUserDefinitionList[1].dd).toEqual(mockTenantUser.tenantUserName)
  })
  })

  describe('filterUsers', () => {
    test('filters a tenant users list by username', async () => {
      const filteredTenantUserList = filterUsers(mockTenantUsers, mockTenantUsers[0].tenantUserName)
      expect(filteredTenantUserList).not.toBeNull()
      expect(filteredTenantUserList[0].tenantUserName).toEqual(mockTenantUsers[0].tenantUserName)
    })
  })
