import React from 'react'
import {Navbar, NavbarBrand} from 'reactstrap'

const Layout = () => (
  <Navbar color='dark' dark>
    <NavbarBrand href='#'>Tenant Manager</NavbarBrand>
  </Navbar>
)

export default Layout
