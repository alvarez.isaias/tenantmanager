import {render, wait} from '@testing-library/react'
import React from 'react'
import {AlertsContextProvider} from '../../../contexts/alerts'
import TenantView from '../TenantView'

describe('<TenantView />', () => {
  test('gets the tenants from the API and sets them on the state', async () => {
    const {getByText} = render(
      <AlertsContextProvider>
        <TenantView />
      </AlertsContextProvider>
    )

    await wait(() => expect(getByText('Add new tenant')).not.toBeNull())
  })
})
