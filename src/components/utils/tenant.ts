import {InputType} from 'reactstrap/lib/Input'
import {
  endpoints,
  http,
  Tenant,
  TenantInput,
} from '../../apiService'
import {DefinitionListElement} from '../shared'

export const getTenantList = async () => {
  try {
    return await http<Tenant[]>(
      `${endpoints.tenants_endpoint}`,
      'GET',
    )
  } catch (err) {
    return err
  }
}

export const addNewTenant = async (newTenant: TenantInput) => {
  try {
    return await http<TenantInput>(
      `${endpoints.tenants_endpoint}`,
      'POST',
      newTenant,
    )
  } catch (err) {
    return err
  }
}

export const tenantPut = async (tenant: Tenant) => {
  try {
    return await http<Tenant>(
      `${endpoints.tenants_endpoint}${tenant.tenantId}`,
      'PUT',
      tenant,
    )
  } catch (err) {
    return err
  }
}

export const getTenantDefinitionList = (tenant: Tenant): DefinitionListElement[] => {
  const list: DefinitionListElement[] = [
    {dt: 'Tenant ID', dd: tenant.tenantId.toString()},
    {dt: 'Active', dd: tenant.isActive ? 'Yes' : 'No'},
    {dd: tenant.tenantName, dt: 'Tenant Name'},
  ]

  if (tenant.isDefault) {
    list[2].badge = {color: 'info', content: 'Default'}
  }

  return list
}

export const tenantInputs = [
  {label: 'Tenant Name', name: 'tenantName', type: 'text' as InputType, required: true},
  {
    label: 'Buyer Email',
    name: 'buyerEmail',
    regex: /\S+@\S+\.\S+/,
    type: 'email' as InputType,
  },
  {
    label: 'Letterhead',
    name: 'letterhead',
    rows: '5',
    type: 'textarea' as InputType,
  },
]
export const tenantChecks = [
  {label: 'Active', name: 'isActive'},
]
