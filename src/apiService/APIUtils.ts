import {APIMethod, IHttpResponse} from './'

export const getMethodHeaders = /* istanbul ignore next */
(method: APIMethod): Headers => {
  const headers = new Headers()
  headers.append('Accept', 'application/json')
  if (method !== 'GET') {
    headers.append('Content-Type', 'application/json')
  }
  return headers
}

export const http = <T>(
  url: string,
  method: APIMethod,
  body?: T,
): Promise<IHttpResponse<T>> => {
  const request = new Request(url, {
    body: JSON.stringify(body),
    headers: getMethodHeaders(method),
    method,
    mode: 'cors',
  })
  /* istanbul ignore next */
  return new Promise(async (resolve, reject) => {
    try {
      const response: IHttpResponse<T> = await fetch(request)
      if (response.status !== 204) {
        response.parsedBody = await response.json()
      }
      resolve(response)
    } catch (err) {
      return err
    }
  })
}
