const strings = {
  editTenantError: 'There was an error editing the tenant',
  editTenantSuccess: 'Tenant edited correctly',
  editTenantUserError: 'There was an error editing the tenant user',
  editTenantUserSuccess: 'Tenant user edited correctly',
  emailError: 'This email is not valid',
  newTenantError: 'There was an error creating the tenant',
  newTenantSuccess: 'Tenant created correctly',
  newTenantUserError: 'There was an error creating the tenant user',
  newTenantUserSuccess: 'Tenant user created correctly',
  requiredError: 'This field is required',
}

export default strings
