import {render} from '@testing-library/react'
import React from 'react'
import {DefinitionListElement} from '../../'
import DefinitionList from '../../DefinitionList/DefinitionList'

const listMockProps = [
  {
      dd: '1st Description',
      dt: '1st Title',
  },
  {
    badge: {
      color: 'blue',
      content: 'test'
    },
    dd: '2nd Description',
    dt: '2nd Title',
  }, {
    dd: '3rd Description',
    dt: '3rd Title',
  }
] as DefinitionListElement[]

describe('<DefinitionList />', () => {
  test('Renders list elements with badges', () => {
    const {getByText} = render(
      <DefinitionList list={ listMockProps }/>
    )

    expect(getByText('1st Title:')).not.toBeNull()
    expect(getByText('1st Description')).not.toBeNull()
    expect(getByText('2nd Title:')).not.toBeNull()
    expect(getByText('2nd Description')).not.toBeNull()
    expect(getByText('test')).not.toBeNull()
    expect(getByText('3rd Title:')).not.toBeNull()
    expect(getByText('3rd Title:')).not.toBeNull()
  })
})
