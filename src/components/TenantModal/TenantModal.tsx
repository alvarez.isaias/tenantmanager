import {cloneDeep, unionBy} from 'lodash'
import React, {useEffect, useState} from 'react'
import {
  Button,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Spinner,
} from 'reactstrap'
import {FormInput} from '../../apiService'
import strings from '../utils/strings'
import {tenantChecks, tenantInputs} from '../utils/tenant'
import {userInputs} from '../utils/tenantUser'

interface TenantModalProps<T> {
  open: boolean
  toggle: () => void
  onSave: (data: T) => void
  saveCallback: () => void
  modalTitle: string
  data: T
  isTenantUser?: boolean
}

const TenantModal = <T extends unknown>({
  open,
  toggle,
  onSave,
  saveCallback,
  modalTitle,
  data,
  isTenantUser,
}: TenantModalProps<T>) => {
  const [currentTenant, setCurrentTenant] = useState<T>({} as T)
  const [loading, setLoading] = useState(false)
  const [inputs, setInputs] = useState<FormInput[]>([])
  const [formValid, setFormValid] = useState(false)
  const handleInputChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    input: FormInput,
  ) => {
    const clonedTenant = cloneDeep(currentTenant)
    const target = event.currentTarget
    const name = target.name
    const isCheck = target.type === 'checkbox'
    const value = isCheck ? target.checked : target.value
    clonedTenant[name] = value
    setCurrentTenant(clonedTenant)
    if (!isCheck) {
      validateInput(input, value)
    }
  }

  const validateInput = (input: FormInput, value: string | boolean) => {
    if (
      (input.required && value === '') ||
      (input.regex && !input.regex.test(value as string))
    ) {
      input.invalid = true
      setFormValid(false)
    } else {
      input.invalid = false
      setFormValid(true)
    }
    setInputs(
      unionBy(inputs, [input], 'name')
    )
  }

  const handleConfirm = async () => {
    setLoading(true)
    await onSave(currentTenant)
    saveCallback()
    setLoading(false)
  }

  useEffect(() => {
    setCurrentTenant(data)
    setInputs(
      isTenantUser ?
        userInputs :
        tenantInputs as FormInput[],
    )
  }, [])

  return (
    <Modal
      isOpen={open}
      toggle={toggle}
      backdrop='static'
    >
      <ModalHeader toggle={toggle}>{modalTitle}</ModalHeader>
      <ModalBody>
        <Form>
          {
            inputs.map((input: FormInput) => (
              <FormGroup key={input.name}>
                <Label for={input.name}>{input.label}</Label>
                <Input
                  {...input}
                  value={currentTenant[input.name] || ''}
                  data-testid={input.name}
                  onChange={(e) => handleInputChange(e, input)}
                />
                {
                  input.required &&
                    <FormFeedback>{strings.requiredError}</FormFeedback>
                }
                {
                  input.regex &&
                    <FormFeedback>{strings.emailError}</FormFeedback>
                }
              </FormGroup>
            ))
          }
          {
            !isTenantUser && tenantChecks.map(check => (
              <FormGroup check key={check.name}>
                <Label check for={check.name}>
                  <Input
                    {...check}
                    type='checkbox'
                    checked={currentTenant[check.name]}
                    onChange={(e) => handleInputChange(e, check)}
                  />
                  {check.label}
                </Label>
              </FormGroup>
            ))
          }
        </Form>
      </ModalBody>
      <ModalFooter>
        {
          loading ? <Spinner type='grow' color='info' /> :
            <>
              <Button color='info' onClick={handleConfirm} disabled={!formValid}>Save</Button>
              <Button color='secondary' onClick={toggle}>Cancel</Button>
            </>
        }
      </ModalFooter>
    </Modal>
  )
}

export default TenantModal
