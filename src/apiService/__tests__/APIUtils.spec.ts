import {TenantInput} from '../'
import {getMethodHeaders, http} from '../APIUtils'
import mockTenants, {postTenantResponse, putTenantInput} from '../__mocks__/mocks'

jest.mock('../APIUtils')

const mockURL = 'http://foo-bar/baz'
const mockTenantInput = new TenantInput()
const httpMock = (http as jest.Mock)
const getMethodHeadersMock = (getMethodHeaders as jest.Mock)

describe('getMethodHeaders', () => {
  test('returns headers for GET request', () => {
    const mockHeaders = [{'Accept': 'application/json'}]
    getMethodHeadersMock.mockReturnValue(mockHeaders)
    expect(getMethodHeaders('GET')).toBe(mockHeaders)
    expect(getMethodHeadersMock).toHaveBeenCalledWith('GET')
  })
  test('returns headers for PUT request', () => {
    const mockHeaders = [
      {'Accept': 'application/json'},
      {'Content-Type': 'application/json'},
    ]
    getMethodHeadersMock.mockReturnValue(mockHeaders)
    expect(getMethodHeaders('PUT')).toBe(mockHeaders)
    expect(getMethodHeadersMock).toHaveBeenCalledWith('PUT')
  })
  test('returns headers for POST request', () => {
    const mockHeaders = [
      {'Accept': 'application/json'},
      {'Content-Type': 'application/json'},
    ]
    getMethodHeadersMock.mockReturnValue(mockHeaders)
    expect(getMethodHeaders('POST')).toBe(mockHeaders)
    expect(getMethodHeadersMock).toHaveBeenCalledWith('POST')
  })
})
describe('http', () => {
  test('makes GET request and returns the data', async () => {
    httpMock.mockReturnValue(Promise.resolve({parsedBody: mockTenants}))
    const data = await http(mockURL, 'GET')

    expect(data.parsedBody).toEqual(mockTenants)
    expect(httpMock).toHaveBeenCalledWith(mockURL, 'GET')
  })
  test('makes POST request', async () => {
    httpMock.mockReturnValue(Promise.resolve({parsedBody: postTenantResponse}))
    const data = await http(mockURL, 'POST', mockTenantInput)

    expect(data.parsedBody).toEqual(postTenantResponse)
    expect(httpMock).toHaveBeenCalledWith(mockURL, 'POST', mockTenantInput)
  })
  test('makes PUT request with no response', async () => {
    httpMock.mockReturnValue(Promise.resolve())
    const data = await http(mockURL, 'PUT', putTenantInput)

    expect(data).toBeUndefined()
    expect(httpMock).toHaveBeenCalledWith(mockURL, 'PUT', putTenantInput)
  })
})
