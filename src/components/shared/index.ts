import DefinitionList from './DefinitionList/DefinitionList'

// Types
export type DefinitionListElement = {
  dt: string
  dd: string
  badge?: DefinitionListBadge
}

export type DefinitionListBadge = {
  color: string
  content: string
}

export {
  DefinitionList,
}
